## MODELE LOGIQUE

entreprise (#SIRET, nom, ville, pays, dateCreation) <br>
> _contrainte :_ <br>
\+ dateCreation < current_date()

 utilisateur (#email, nom, prénom, dateNaissance, genre, pays, metier) <br>
> _contraintes :_ <br>
\+ not null(entreprise) <br>
\+ dateNaissance < current_date()

responsableFlux (#email ➔ utilisateur) <br>
> _contrainte :_ <br>
\+ projection(responsableFlux(email)) = projection(flux(responsable))

groupe (#nom, type, responsable ➔ utilisateur, entreprise ➔ entreprise) <br>
> _contrainte :_ <br>
\+ not null(responsable)

flux (#nom, description, dateCreation, responsable ➔ responsableFlux) <br>
> _contrainte :_ <br>
\+ not null(responsable)

publication (#id, titre, description, lienImage, date, flux ➔ flux, utilisateur ➔ utilisateur) <br>
> _contraintes :_ <br>
\+ not null(flux) <br>
\+ not null(utilisateur) <br>
\+ utilisateur appartient à un groupe rédacteur du flux

noteResponsable (#id, titre, description, lienImage, date, flux ➔ flux, responsable ➔ responsableFlux) <br>
> _contraintes :_ <br>
\+ not null(flux) <br>
\+ not null(responsable) <br>
\+ intersection(projection(publication, id), projection(noteResponsable, id)) = {}

vote (#publication ➔ publication, #utilisateur ➔ utilisateur, contenu, date) <br>

commentaire (#num, #publication ➔ publication, utilisateur ➔ utilisateur, contenu, date) <br>
> _contrainte :_ <br>
\+ not null(utilisateur)

message (#num, contenu, date, #discussion ➔ discussion) <br>
> _contrainte :_ <br>
\+ not null(discussion)

discussion (#id, utilisateur1 ➔ utilisateur, utilisateur2 ➔ utilisateur) <br>
> _contrainte :_ <br>
\+ utilisateur1 ≠ utilisateur2

groupe_utilisateur (#utilisateur  ➔ utilisateur, #groupe ➔ groupe) <br>
> _contraintes :_ <br>
\+ projection(groupe(nom)) = projection(groupe_utilisateur(nom)) <br>
\+ projection(utilisateur(groupe)) = projection(groupe_utilisateur(groupe)) <br>
\+ tous les groupes auxquels appartient un utilisateur doivent être des groupes de la même entreprise

groupe_flux (#flux  ➔ flux, #groupe ➔ groupe) <br>
> _contraintes :_ <br>
\+ R1 = restriction(groupe, type = "Lecteur") <br>
projection(R1(nom)) = projection(groupe_flux(flux)) <br>
\+ R2 = restriction(groupe, type = "Rédacteur") <br>
projection(R2(nom)) = projection(flux(flux))
