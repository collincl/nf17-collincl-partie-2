## NOTE DE CLARIFICATION

LISTE DES OBJETS

\+ Les utilisateurs ont un email, un nom, un prénom, une date de naissance (i.e. on peut calculer leur âge), un genre, un métier et habitent dans un pays. Parmi les utilisateurs, il y a des responsables flux et des responsables groupe.

\+ Les entreprises ont un siret, un nom, une date de création et leur siège social se situe dans une ville, dans un pays.

\+ Les groupes ont un nom. Il y a deux types de groupes : les groupes lecteurs et les groupes rédacteurs.

\+ Les parutions ont un titre, une date de mise en ligne et peuvent contenir une image ou une description ou les deux. On distingue deux types de parutions : les publications et les notes responsable.

\+ Les flux de parutions ont un titre, une date de création et peuvent avoir une description.

\+ Les commentaires ont un contenu (texte) et une date.

\+ Les votes ont un contenu ("like" ou "dislike") et une date.

\+ Les messages ont un contenu (texte) et une date.

\+ Les discussions.

LISTE DES ASSOCIATIONS

\+ Les groupes sont des groupes d'une entreprise. Les entreprises peuvent avoir autant de groupes qu'elles le souhaitent.

\+ Les utilisateurs font partie de groupes (au moins un groupe). Les groupes peuvent contenir de nombreux utilisateurs.

\+ Chaque groupe est géré par un responsable groupe.

\+ Les groupes donnent accès à des flux. Chaque flux possède au minimum un groupe lecteur et un groupe rédacteur.

\+ Chaque flux est géré par un responsable flux.

\+ Les flux contiennent des parutions. Une parution est contenue dans un seul flux.

\+ Chaque publication est mise en ligne par un utilisateur. Il n'y a pas de nombre maximum de publications par utilisateur.

\+ Chaque note responsable est mise en ligne par un responsable flux. Il n'y a pas de nombre maximum de notes responsable par responsable flux.

\+ Chaque commentaire appartient à une publication. Chaque publication peut être commentée de nombreuses fois.

\+ Chaque vote se rapporte à une publication. Chaque publication peut être like/dislike "un nombre illimité" de fois.

\+ Chaque commentaire est laissé par un utilisateur. Un utilisateur peut laisser de nombreux commentaires.

\+ Chaque vote est laissé par un utilisateur. Un utilisateur ne peut voter qu'une fois par publication.

\+ Deux utilisateurs peuvent avoir des discussions.

\+ Les discussions contiennent 1 ou plusieurs messages.

<!-- -- A faire

\+ utilisateur appartient à un groupe rédacteur du flux

\+ utilisateur1 != utilisateur2 (message)

\+ on peut calculer leur âge

\+ Pour chaque _utilisateur_, une vue doit permettre d'afficher son "home" ou "profile".

\+ Pour chaque _utilisateur_, une vue doit permettre d'afficher son dashboard, c'est-à-dire lui présenter tous les flux qu'il peut lire. -->
